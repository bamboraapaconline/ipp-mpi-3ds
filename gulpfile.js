// This is a task runner for the NewDC UI Automated Tests
const path = require('path');
const gulp = require('gulp');
const selenium = require('selenium-standalone');
const webdriver = require('gulp-webdriver');
const exec = require("child_process").exec;
gulp.task('selenium', function(done) {
  selenium.install({}, function(err) {
    if (err) return done(err);
    selenium.start(function(err, child) {
      if (err) return done(err);
      selenium.child = child;
      done();
    });
  });
});

function runFunctionalTests(done) {
  LogSpecs();
  return gulp
    .src(path.join(__dirname, 'wdio.conf.js'))
    .pipe(webdriver())
    .once('error', () => process.exit(1))
    .once('error', () => {
      selenium.child.kill();
    })
    .once('end', () => {
      selenium.child.kill();
    });
  done();
}

gulp.task('clearselenium', function(done) {
  exec('Powershell.exe Stop-Process -Name "*2.40-x64-chromedriver"');
  done();
});

gulp.task('clearoutput', function (done) {
  require('../Common/Common.js').clearOutputFolder();
  done();
});

gulp.task('startIPPMPI3DSTests', gulp.series('selenium', function APTesting(done) {
    AddIPPMPI3DSTest();
  return runFunctionalTests(done);
}));

function AddIPPMPI3DSTest() {
  var specs = require('./wdio.conf.js').config.specs;
  specs.push('./Tests/IPPMPI3DS/IPPMPI3DS_Specs.js');
}

function LogSpecs() {
  console.log(require('./wdio.conf.js').config.specs);
}
