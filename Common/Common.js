var APVariablesListObject = require('../Variable/IPPMPI3DSVariablesListObject.js');
var apVarList = new APVariablesListObject();
var fs = require('fs');
var dateFormat = require('dateformat');
const exec = require("child_process").exec;
var path = require('path');
const wdioconf = require('../wdio.conf.js');
const cheerio = require('cheerio');
var urlencdec = require('urlencode');
var base64 = require('base-64');
var common = require('./Common.js');// in case functions with the module are called with module name
var assert = require('assert');
var CryptoJS = require("crypto-js");
var key = 'wZVoStxqaP';
var QueryResult;

function CreateFolders(folderpath) {
  try {
    var mkdirp = require('mkdirp');
    mkdirp.sync(folderpath, function (err) {
      if (err) console.log(err);
      else {
        console.log(`folder ${folderpath} Created!`);
      }
    });
  }
  catch (err) {
    console.log('Error: ' + err);
  }
}

module.exports.setgsvariables = setGSvariables;
function setGSvariables(gsvariableslistobject, varList) {
  GSVariablesListObject = gsvariableslistobject;
  apVarList = varList;
}

module.exports.launchAPUrl = function launchUrl(commonTestData, specificTestData) {
  var URL = commonTestData.AccessPortalUrl;
  var SessionID = Math.random().toString(36).replace('0.', '');
  var decrypted = CryptoJS.AES.decrypt(specificTestData.Password, key);
  var Password = decrypted.toString(CryptoJS.enc.Utf8);
  var requestURL = URL + "?UserName=" + specificTestData.UserName + "&Password=" + Password + "&SessionID=" + SessionID + "&SessionKey=12345" +
    "&DL=" + specificTestData.DL + "&Amount=" + specificTestData.Amount +
    "&CustRef=" + specificTestData.CustRef + "&CustNumber=" + specificTestData.CustNumber;  
    browser.url(requestURL);
    browser.pause(3000);
    var SessionStoredError = browser.getValue(apVarList.sessionstorederror);
    assert.equal(SessionStoredError.length, 0, `SessionStoredError: ${SessionStoredError}`);
    var SST = browser.getValue(apVarList.hiddensst);
    var sessionIDSSTRequestURL = URL + "?SessionID=" + SessionID + "&SST=" + SST;
    browser.url(sessionIDSSTRequestURL);
    QueryResult = URL + "?UserName=" + specificTestData.UserName + "&Password=" + Password + "&SessionID=" + SessionID + "&Query=True" + "&SST=" + SST;
    browser.pause(3000);
    return true;
};

/*module.exports.launchApRedUrl = function launchUrl(commonTestData, specificTestData) {
  var URL = commonTestData.AccessPortalUrl;
  var SessionID = Math.random().toString(36).replace('0.', '');
  var decrypted = CryptoJS.AES.decrypt(specificTestData.Password, key);
  var Password = decrypted.toString(CryptoJS.enc.Utf8);
  var requestURL = URL + "?UserName=" + specificTestData.UserName + "&Password=" + Password + "&SessionID=" + SessionID + "&SessionKey=12345" +
    "&DL=" + specificTestData.DL + "&Amount=" + specificTestData.Amount +
    "&CustRef=" + specificTestData.CustRef + "&CustNumber=" + specificTestData.CustNumber + "&Reference4=" + specificTestData.Reference4 + "&Reference5=" + specificTestData.Reference5;  
    browser.url(requestURL);
    browser.pause(3000);
    var SessionStoredError = browser.getValue(apVarList.sessionstorederror);
    assert.equal(SessionStoredError.length, 0, `SessionStoredError: ${SessionStoredError}`);
    var SST = browser.getValue(apVarList.hiddensst);
    var sessionIDSSTRequestURL = URL + "?SessionID=" + SessionID + "&SST=" + SST;
    browser.url(sessionIDSSTRequestURL);
    QueryResult = URL + "?UserName=" + specificTestData.UserName + "&Password=" + Password + "&SessionID=" + SessionID + "&Query=True" + "&SST=" + SST;
    browser.pause(3000);
    return true;
};*/

/*module.exports.launchApPaypalUrl = function launchUrl(commonTestData, specificTestData) {
  var URL = commonTestData.AccessPortalUrl;
  var SessionID = Math.random().toString(36).replace('0.', '');
  var decrypted = CryptoJS.AES.decrypt(specificTestData.Password, key);
  var Password = decrypted.toString(CryptoJS.enc.Utf8);
  var requestURL = URL + "?UserName=" + specificTestData.UserName + "&Password=" + Password + "&SessionID=" + SessionID + "&SessionKey=12345" +
    "&DL=" + specificTestData.DL + "&Amount=" + specificTestData.Amount +
    "&CustRef=" + apVarList.PaypalCustref + "&CustNumber=" + specificTestData.CustNumber + 
    "&PP_Reference1=" + specificTestData.PP_Reference1 + "&PP_Reference2=" + specificTestData.PP_Reference2 + "&PP_Reference3=" + specificTestData.PP_Reference3 + "&PP_Reference4=" + specificTestData.PP_Reference4;  
    browser.url(requestURL);
    browser.pause(3000);
    var SessionStoredError = browser.getValue(apVarList.sessionstorederror);
    assert.equal(SessionStoredError.length, 0, `SessionStoredError: ${SessionStoredError}`);
    var SST = browser.getValue(apVarList.hiddensst);
    var sessionIDSSTRequestURL = URL + "?SessionID=" + SessionID + "&SST=" + SST;
    browser.url(sessionIDSSTRequestURL);
    QueryResult = URL + "?UserName=" + specificTestData.UserName + "&Password=" + Password + "&SessionID=" + SessionID + "&Query=True" + "&SST=" + SST;
    browser.pause(3000);
    return true;
}; */

/* module.exports.launchRbaAtoUrl = function launchUrl(commonTestData, specificTestData) {
  var URL = commonTestData.AccessPortalUrl;
  var SessionID = Math.random().toString(36).replace('0.', '');
  var decrypted = CryptoJS.AES.decrypt(specificTestData.Password, key);
  var Password = decrypted.toString(CryptoJS.enc.Utf8);
  var requestURL = URL + "?UserName=" + specificTestData.UserName + "&Password=" + Password + "&SessionID=" + SessionID + "&SessionKey=12345" +
    "&DL=" + specificTestData.DL;
    browser.url(requestURL);
    browser.pause(3000);
    var SessionStoredError = browser.getValue(apVarList.sessionstorederror);
    assert.equal(SessionStoredError.length, 0, `SessionStoredError: ${SessionStoredError}`);
    var SST = browser.getValue(apVarList.hiddensst);
    var sessionIDSSTRequestURL = URL + "?SessionID=" + SessionID + "&SST=" + SST;
    browser.url(sessionIDSSTRequestURL);    
    browser.pause(3000);
    return true;
}; */

module.exports.queryresults = function queryresults() {
  browser.url(QueryResult);
}
/*
module.exports.enterPaypalDetails = enterPaypalDetails;
function enterPaypalDetails(PPUserName, PPPassword) {
  let alltabids = browser.getTabIds();
  var numberOfTabs = alltabids.length;
  browser.switchTab(alltabids[numberOfTabs - 1]);
  browser.waitForVisible(apVarList.PaypalUsername);   
  browser.clearElement(apVarList.PaypalUsername);
  browser.element(apVarList.PaypalUsername).setValue(PPUserName);
  browser.waitForVisible(apVarList.PaypalPassword);
  browser.clearElement(apVarList.PaypalPassword);
  browser.setValue(apVarList.PaypalPassword, PPPassword);
  browser.click(apVarList.PaypalLogin);
  browser.pause(10000); 
}
*/
module.exports.enterCardAndMakePayment = enterCardAndMakePayment;
function enterCardAndMakePayment(CardHolderName, CardNumber, ExpM, ExpY, CVV) {
  let alltabids = browser.getTabIds();
  var numberOfTabs = alltabids.length;
  browser.switchTab(alltabids[numberOfTabs - 1]);
  browser.waitForVisible(apVarList.cardholdername);
  browser.setValue(apVarList.cardholdername, CardHolderName);
  browser.setValue(apVarList.cardnumber, CardNumber);
  browser.selectByValue(apVarList.expm, ExpM);
  browser.selectByValue(apVarList.expy, ExpY);
  browser.setValue(apVarList.cvv, CVV);
  browser.click(apVarList.paynow);
  browser.pause(1000);
  return true;
}

module.exports.enterDetailsAndPay = enterDetailsAndPay;
function enterDetailsAndPay(Prnnumber,Amount,cardnumber,ExpM,ExpY,CCV) {
  let alltabids = browser.getTabIds();
  var numberOfTabs = alltabids.length;
  browser.switchTab(alltabids[numberOfTabs - 1]);  
  browser.setValue(apVarList.PRNNumber, Prnnumber);
  browser.setValue(apVarList.PayAmount, Amount);
  browser.setValue(apVarList.cardnumber, cardnumber);
  browser.selectByValue(apVarList.expm, ExpM);
  browser.selectByValue(apVarList.expy, ExpY);
  browser.setValue(apVarList.ccv, CCV);
  browser.click(apVarList.calculatefee);
  browser.pause(5000);
  browser.click(apVarList.PayButton);
  browser.pause(5000);
  return true;
}

module.exports.make3DSTransaction = function () {
  browser.frame(0);
    //browser.click(gcVarlist.threeDSPayNow); 
    var isThreeDSbutton = browser.isExisting(apVarList.threeDSPayNow);
    if (isThreeDSbutton)
    {
      //console.log(gcVarlist.threeDSPayNow);
      browser.click(apVarList.threeDSPayNow);
    }
    else{
      console.log("Service Unavailable");
    }
};

module.exports.getQueryResults = getQueryResults;
function getQueryResults() {  
  var resultvalue = browser.getHTML(apVarList.queryresultbox);
  // receipt     
  var patternReceipt = /Receipt\"\svalue\=\"(\w+)\"/;  
  var matchReceipt = resultvalue.match(patternReceipt);    
  if (matchReceipt) {
    // Result
    let patternResult = /Result\"\svalue\=\"(\w+)\"/;
    let matchResult = resultvalue.match(patternResult);
    assert.equal(matchResult[1], 1);

    // DeclinedCode
    let patternDeclinedCode = /DeclinedCode\"\svalue\=\"(\w+)\"/;
    let matchDeclinedCode = resultvalue.match(patternDeclinedCode);
    assert.equal(matchDeclinedCode, null);
   }

  // Token
  let patternToken = /Token\"\svalue\=\"(\w+)\"/;
  let matchToken = resultvalue.match(patternToken);
  
  // CardSubType
  let patternCardSubType = /CardSubType\"\svalue\=\"(\w+)\"/;
  let matchCardSubType = resultvalue.match(patternCardSubType);
  
  // RedResponse
  let patternRedResponse = /ReDShieldResponse\"\svalue\=\"(\w+.*)\"/;
  let matchRedResponse = resultvalue.match(patternRedResponse);
  
  return resultvalue;
}

module.exports.writeTestResultsToFile = writeTestResultsToFile;
function writeTestResultsToFile(OutputResponseFileName, trnCardResult, environment) {
  var dir = wdioconf.config.outputtestresults;
  // get folder path from the OutputResponseFileName e.g. giftcard/TestGiftcardwithoutSavedCard
  var filepath = path.join(dir, OutputResponseFileName + dateFormat(Date(), '_yyyymmdd_hhMMss_') + environment + ".txt");
  CreateFolders(path.dirname(filepath)); // create folder hierarchy
  fs.writeFile(filepath, trnCardResult, function (err) {
    if (err) {
      console.log("Error while saving file: " + err);
    }
    else {
      return;
    }
  });
}

/*module.exports.enterBankAccountDetails = enterBankAccountDetails;
function enterBankAccountDetails(BankAccountHolderName, BSB, BankAccountNumber) {
  let alltabids = browser.getTabIds();
  var numberOfTabs = alltabids.length;
  browser.switchTab(alltabids[numberOfTabs - 1]);
  browser.waitForVisible(apVarList.bankAccountHolderName);
  browser.setValue(apVarList.bankAccountHolderName, BankAccountHolderName);
  browser.setValue(apVarList.bankAccountBSB, BSB);
  browser.setValue(apVarList.bankAccountNumber, BankAccountNumber);
  browser.click(apVarList.paynow);
  browser.pause(1000);
}*/

module.exports.crmLogon = crmLogon;
function crmLogon(CrmUrl, UserName, Password) {
  var key = 'wZVoStxqaP';
  var decrypted = CryptoJS.AES.decrypt(Password, key);
  var Password = decrypted.toString(CryptoJS.enc.Utf8);
  console.log = (Password);
  browser.url(CrmUrl);
  browser.waitForVisible(apVarList.crmUserName);
  browser.setValue(apVarList.crmUserName, UserName);
  browser.setValue(apVarList.crmPassword, Password);
  browser.click(apVarList.crmLogonButton);
}

module.exports.selectCompany = selectCompany;
function selectCompany(Company) {
  browser.changeFrame(apVarList.crmframecontents);
  browser.changeFrame(apVarList.crmframetoolbar);
  browser.click(apVarList.selectcompanytag);
  browser.waitForVisible(`[value="${Company}"]`);
  browser.click(`[value="${Company}"]`);
}

module.exports.selectAccount = selectAccount;
function selectAccount (AccountPath, Account, TreeViewTagId) {
  browser.changeFrame(apVarList.crmframemainmenu);
  browser.click(TreeViewTagId);
  browser.frameParent();
  browser.changeFrame(apVarList.crmframecontents);
  browser.changeFrame(apVarList.crmframecontenttop);
  clickThroughElementsPath(AccountPath);
  browser.frameParent();
  browser.changeFrame(apVarList.crmframecontentbottom);
  var AccountId = browser.getIDTag(Account);
  browser.click(AccountId);
}

module.exports.clickThroughElementsPath = clickThroughElementsPath;
function clickThroughElementsPath (AccountPath) {
  var Pathes = AccountPath.split("|");
  for (i = 0; i < Pathes.length; i++) {
    var elementId = browser.getIDTag(Pathes[i]);
    browser.click(elementId);
  }
}

module.exports.searchPayment = searchPayment;
  function searchPayment (receipt,ccfirst6,cclast4,PaymentType,PaymentStatus){  
    var spliturl = browser.getUrl().split(apVarList.crmIndexUrl);
    browser.url(spliturl[0] + apVarList.crmpaymenthistory);
    browser.setValue(apVarList.prmreceipt, receipt);        
    browser.click(apVarList.crmpaysearchchk);    
    browser.click(apVarList.crmpayccsearch);          
    browser.selectByVisibleText(apVarList.selectpaymentactions, "View Details"); 
  }
  /*
  module.exports.refundPayment = refundPayment;
  function refundPayment (receipt){  
    var spliturl = browser.getUrl().split(apVarList.crmIndexUrl);
    browser.url(spliturl[0] + apVarList.crmpaymenthistory);
    browser.setValue(apVarList.prmreceipt, receipt);        
    browser.click(apVarList.crmpaysearchchk);    
    browser.click(apVarList.crmpayccsearch);          
    browser.selectByVisibleText(apVarList.selectpaymentactions, "Refund Trn.");
    browser.click(apVarList.submitRefundbtn);
  }

  module.exports.partialRefundPayment = partialRefundPayment;
  function partialRefundPayment (receipt,amount){  
    var spliturl = browser.getUrl().split(apVarList.crmIndexUrl);
    browser.url(spliturl[0] + apVarList.crmpaymenthistory);
    browser.setValue(apVarList.prmreceipt, receipt);      
    browser.click(apVarList.crmpaysearchchk);    
    browser.click(apVarList.crmpayccsearch);          
    browser.selectByVisibleText(apVarList.selectpaymentactions, "Refund Trn.");
    browser.setValue(apVarList.partialrefundamt, amount);
    browser.click(apVarList.submitRefundbtn);
  }

  module.exports.voidPayment = voidPayment;
  function voidPayment (receipt){  
    var spliturl = browser.getUrl().split(apVarList.crmIndexUrl);
    browser.url(spliturl[0] + apVarList.crmpaymenthistory);
    browser.setValue(apVarList.prmreceipt, receipt);        
    browser.click(apVarList.crmpaysearchchk);    
    browser.click(apVarList.crmpayccsearch);          
    browser.selectByVisibleText(apVarList.selectpaymentactions, "Void Trn.");
    browser.click(apVarList.submitvoidbtn);
  } */

module.exports.toggleCardFeaturesCheckBoxes = toggleCardFeaturesCheckBoxes;
function toggleCardFeaturesCheckBoxes (ExpectedDifferentialChargeStatus, ExpectedBinLookupStatus) {
  // Navigate directly to Card feature Config page at this point. It saves a lot of action in changing to different frames and searching the correct ID/elements
  var spliturl = browser.getUrl().split(apVarList.crmIndexUrl);
  browser.url(spliturl[0] + apVarList.crmCardFeatures_ConfigUrl);
  browser.waitForVisible(apVarList.crmDifferentialSurchargeCheckBoxId);
  var CurrentDifferentialChargeStatus = browser.element(apVarList.crmDifferentialSurchargeCheckBoxId);

  var clickSaveBtnRequired = false;
  // If the current status of the check box does not equal to the expected status then toggle the status by clicking the check box
  if (CurrentDifferentialChargeStatus.isSelected() != ExpectedDifferentialChargeStatus) {
    browser.click(apVarList.crmDifferentialSurchargeCheckBoxId);
    clickSaveBtnRequired = true;
  }

  browser.waitForVisible(apVarList.crmDifferentialSurchargeCheckBoxId);
  var CurrentBinLookupStatus = browser.element(apVarList.crmBinLookupCheckBoxId);
  if (CurrentBinLookupStatus.isSelected() != ExpectedBinLookupStatus)
  {
    browser.click(apVarList.crmBinLookupCheckBoxId);
    clickSaveBtnRequired = true;
  }

  if (clickSaveBtnRequired) {
    browser.click(apVarList.crmCardFeaturesConfigSaveBtn);
  }

  browser.url(spliturl[0] + apVarList.crmIndexUrl);
}

module.exports.reloadDTS = reloadDTS;
function reloadDTS(FindTrnTagId) {
  browser.changeFrame(apVarList.crmframemainmenu);
  browser.click(FindTrnTagId);
  browser.frameParent();
  browser.changeFrame(apVarList.crmframecontents);
  browser.changeFrame(apVarList.crmframecontenttop);
  var source = browser.getSource();
  browser.click('#btnReloadDTS');
  browser.waitForVisible('#labDTSReloadSuccess');
}