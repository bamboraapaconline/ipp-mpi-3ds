var path = require('path');

const pathToDownload = path.resolve('chromeDownloads');

exports.config = {
  specs: [
    /** ***DONE**************/
    // './specs/POC_ReadJSONDataAndExecute.js', // done
  ],
  exclude: [
    // 'path/to/excluded/files'
  ],
  /** *****this is for local testing******/
  maxInstances: 1,
  capabilities: [
    {
      maxInstances: 1,
      browserName: 'chrome',
      // ~ No interaction is showing on the browser
      // chromeOptions: {
      //   args: ['--headless', '--disable-gpu']
      // }
      // ~ No interaction is showing on the browser
      // },
      // {
      //   maxInstances: 1,
      //   browserName: 'firefox'
    }
  ],
  /**************************************/
  sync: true,
  // logLevel: 'verbose',
  logLevel: 'silent',
  coloredLogs: true,
  //
  // If you only want to run your tests until a specific amount of tests have failed use // bail (default is 0 - don't bail, run all tests).
  bail: 0,
  screenshotPath: './Output/TemporaryFiles',
  outputtestresults: './Output/LogFiles',
  baseUrl: '/',
  waitforTimeout: 50000,
  connectionRetryTimeout: 50000,
  connectionRetryCount: 3,
  framework: 'mocha',
  // reporters: ['dot','json'],
  // reporters: ['spec', 'allure'],
  reporters: ['spec', 'mochawesome'],
  //reporters: ['spec', 'json'],  
  reporterOptions: {     
    mochawesome:{
       outputDir: './',              
       mochawesome_filename: 'TestResult.json'        
    }
  },
  mochaOpts: {
    ui: 'bdd',
    timeout: 99999,
  },
  before: function() {
    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    // chai.use(chaiAsPromised);
    // chai.should();
    global.expect = chai.expect;
    browser.windowHandleMaximize();
  },
  afterTest: function(event) {
    // var chalkrainbow = require('chalk-rainbow');
    // console.log(chalkrainbow(` Passed: [${event.passed}]`));
  },
  after: function(failures, pid) {},
  onComplete: function() {}
};
