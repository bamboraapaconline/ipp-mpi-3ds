var APTestData;
var argv = require('yargs').argv;
// if (argv.env == undefined) {
//   APTestData = require('../../Input/AccessPortal_RegTests_Demo');
// }
// else {
//   APTestData = require('../../Input/AccessPortal_RegTests_' + argv.env);
// }

var program = require('commander');
program.option('-e, --environment <env>', 'Environment').parse(process.argv);

const Environments = {
  Dev: 'Dev',
  Demo: 'Demo',
  Prod: 'Prod',
  AU1Prod: 'AU1Prod',
  AU2Prod: 'AU2Prod'
}

if (program.environment == Environments.Dev) {
  APTestData = require('../../Input/IPPMPI3DS_Dev');
}
else if(program.environment == Environments.Demo) {
  APTestData = require('../../Input/IPPMPI3DS_Demo');
}
else if(program.environment == Environments.Prod) {
  APTestData = require('../../Input/IPPMPI3DS_Prod');
}
else if(program.environment == Environments.AU1Prod) {
  APTestData = require('../../Input/IPPMPI3DS_AU1Prod');
}
else if(program.environment == Environments.AU2Prod) {
  APTestData = require('../../Input/IPPMPI3DS_AU2Prod');
}

function getTestModulePath(ExecutionJSFile) {
  return `../../Tests/IPPMPI3DS/${ExecutionJSFile}`;
}

describe('IPPMPI3DS Tests', function () {
  before('healthcheck', function() {
    browser.url(APTestData.TestDetails[0].CommonTestData[0].AccessPortalUrl);
  });

  before(function() {
    browser.addCommand('changeFrame', function(frametag) {
      browser.waitForVisible(frametag);
      var framename = $(frametag).value;
      browser.frame(framename);
    });
    browser.addCommand('getIDTag', function(tagid) {
      var source = browser.getSource();
      var pattern = new RegExp('(?:id=")(\\w+)(?:".+' + tagid + ')');
      var matchvalue = source.match(pattern);
      var tagid = `#${matchvalue[1]}`;
      return tagid;
    });
  });

  for (var testsCount = 0; testsCount < APTestData.TestDetails.length; testsCount++) {
    (function (testsCount) {
      var commonTestData = APTestData.TestDetails[testsCount].CommonTestData[testsCount];
      var recordCommonTestDataCount = APTestData.TestDetails[testsCount].CommonTestData.length;
      var recordSpecificTestDataCount = APTestData.TestDetails[testsCount].SpecificTestData.length;
      for (var i = 0; i < recordSpecificTestDataCount; i++) {
        (function (i) {
          var SpecificTestData = APTestData.TestDetails[testsCount].SpecificTestData[i];
          var testModulePath = getTestModulePath(SpecificTestData.ExecutionJSFile);
          var ExecutionJSFile = require(testModulePath);
          it(SpecificTestData.TestDetails, function () {
            ExecutionJSFile[SpecificTestData.ExecutionJSFunction](commonTestData, SpecificTestData, argv.environment);
          });
        }).call(this, i);
      }
    }).call(this, testsCount);
  }
});
