var APVariablesListObject = require('../../Variable/IPPMPI3DSVariablesListObject.js');
var apVarlist = new APVariablesListObject();
var common = require('../../Common/Common.js');
// var assert = require('assert');
var CryptoJS = require("crypto-js");
var key = 'wZVoStxqaP';

/*module.exports.StandardPayment = StandardPayment;
function StandardPayment(commonTestData, specificTestData, env) {
  common.setgsvariables(APVariablesListObject, apVarlist);
  common.launchAPUrl(commonTestData, specificTestData);
  browser.waitForVisible(apVarlist.paynow);
  common.enterCardAndMakePayment(specificTestData.CardholderName, specificTestData.CardNumber, specificTestData.ExpM, specificTestData.ExpY, specificTestData.CVV);
  common.queryresults();
  var trnResult = common.getQueryResults();
  common.writeTestResultsToFile(specificTestData.OutputResponseFileName, trnResult, env);
}

module.exports.StandardPaymentwithRefund = StandardPaymentwithRefund;
  function StandardPaymentwithRefund(commonTestData, specificTestData, env) {
    common.setgsvariables(APVariablesListObject, apVarlist);
    common.launchAPUrl(commonTestData, specificTestData);
    browser.waitForVisible(apVarlist.paynow);
    common.enterCardAndMakePayment(specificTestData.CardholderName, specificTestData.CardNumber, specificTestData.ExpM, specificTestData.ExpY, specificTestData.CVV);    
    common.queryresults();
    var trnResult = common.getQueryResults();    
    var receiptPattern = /Receipt\"\svalue\=\"(\w+)\"/;  
    var receiptNo = (trnResult.match(receiptPattern)).toString().split(",");  
    var receiptNum = (receiptNo[1]);           
    common.writeTestResultsToFile(specificTestData.OutputResponseFileName, trnResult, env);
    common.crmLogon(commonTestData.CrmUrl, commonTestData.CrmUserName, commonTestData.CrmPassword);
    common.selectCompany(specificTestData.CompanyID);
    common.selectAccount(specificTestData.AccountPath, specificTestData.AccountID, specificTestData.TreeViewTagId);
    common.refundPayment(receiptNum);
}

module.exports.StandardPaymentwithPartialRefund = StandardPaymentwithPartialRefund;
  function StandardPaymentwithPartialRefund(commonTestData, specificTestData, env) {
    common.setgsvariables(APVariablesListObject, apVarlist);
    common.launchAPUrl(commonTestData, specificTestData);
    browser.waitForVisible(apVarlist.paynow);
    common.enterCardAndMakePayment(specificTestData.CardholderName, specificTestData.CardNumber, specificTestData.ExpM, specificTestData.ExpY, specificTestData.CVV);    
    common.queryresults();
    var trnResult = common.getQueryResults();    
    var receiptPattern = /Receipt\"\svalue\=\"(\w+)\"/;  
    var receiptNo = (trnResult.match(receiptPattern)).toString().split(",");  
    var receiptNum = (receiptNo[1]);   
    var refundamount = (specificTestData.Amount/200).toFixed(2);            
    common.writeTestResultsToFile(specificTestData.OutputResponseFileName, trnResult, env);
    common.crmLogon(commonTestData.CrmUrl, commonTestData.CrmUserName, commonTestData.CrmPassword);
    common.selectCompany(specificTestData.CompanyID);
    common.selectAccount(specificTestData.AccountPath, specificTestData.AccountID, specificTestData.TreeViewTagId);    
    common.partialRefundPayment(receiptNum,refundamount);
}

module.exports.StandardPaymentwithVoid = StandardPaymentwithVoid;
  function StandardPaymentwithVoid(commonTestData, specificTestData, env) {
    common.setgsvariables(APVariablesListObject, apVarlist);
    common.launchAPUrl(commonTestData, specificTestData);
    browser.waitForVisible(apVarlist.paynow);
    common.enterCardAndMakePayment(specificTestData.CardholderName, specificTestData.CardNumber, specificTestData.ExpM, specificTestData.ExpY, specificTestData.CVV);    
    common.queryresults();
    var trnResult = common.getQueryResults();      
    var receiptPattern = /Receipt\"\svalue\=\"(\w+)\"/;  
    var receiptNo = (trnResult.match(receiptPattern)).toString().split(",");  
    var receiptNum = (receiptNo[1]);             
    common.writeTestResultsToFile(specificTestData.OutputResponseFileName, trnResult, env);
    common.crmLogon(commonTestData.CrmUrl, commonTestData.CrmUserName, commonTestData.CrmPassword);
    common.selectCompany(specificTestData.CompanyID);
    common.selectAccount(specificTestData.AccountPath, specificTestData.AccountID, specificTestData.TreeViewTagId);
    common.voidPayment(receiptNum);
}

module.exports.StandardPaymentRed = StandardPaymentRed;
function StandardPaymentRed(commonTestData, specificTestData, env) {
  common.setgsvariables(APVariablesListObject, apVarlist);
  common.launchApRedUrl(commonTestData, specificTestData);
  browser.waitForVisible(apVarlist.paynow);
  common.enterCardAndMakePayment(specificTestData.CardholderName, specificTestData.CardNumber, specificTestData.ExpM, specificTestData.ExpY, specificTestData.CVV);
  common.queryresults();
  var trnResult = common.getQueryResults();
  common.writeTestResultsToFile(specificTestData.OutputResponseFileName, trnResult, env);
}*/

module.exports.DirectDebit = function (commonTestData, specificTestData, env) {
  common.setgsvariables(APVariablesListObject, apVarlist);
  common.launchAPUrl(commonTestData, specificTestData);
  browser.waitForVisible(apVarlist.paynow);
  common.enterBankAccountDetails(specificTestData.BankAccountHolderName, specificTestData.BankAccountBSB, specificTestData.BankAccountNumber);
  common.queryresults();
  var trnResult = common.getQueryResults();
  common.writeTestResultsToFile(specificTestData.OutputResponseFileName, trnResult, env);
};

module.exports.SurchargeTest = function (commonTestData, specificTestData, env) {
  common.crmLogon(commonTestData.CrmUrl, commonTestData.CrmUserName, commonTestData.CrmPassword);
  common.selectCompany(specificTestData.CompanyID);
  common.selectAccount(specificTestData.AccountPath, specificTestData.AccountID, specificTestData.TreeViewTagId);
  var isSurchargeON = (specificTestData.DifferentialSurchargeStatus == 'ON');
  var isBinLookupON = (specificTestData.BinLookupStatus == 'ON');
  common.toggleCardFeaturesCheckBoxes(isSurchargeON, isBinLookupON);
  common.selectCompany(commonTestData.IPPaymentsCompanyID);
  common.reloadDTS(commonTestData.IPPaymentFindTrnTagID);
  StandardPayment(commonTestData, specificTestData, env);
};

module.exports.StandardThreeDSPayment = StandardThreeDSPayment;
function StandardThreeDSPayment(commonTestData, specificTestData, env) {
  common.setgsvariables(APVariablesListObject, apVarlist);
  common.launchAPUrl(commonTestData, specificTestData);
  browser.waitForVisible(apVarlist.paynow);
  common.enterCardAndMakePayment(specificTestData.CardholderName, specificTestData.CardNumber, specificTestData.ExpM, specificTestData.ExpY, specificTestData.CVV);
  common.make3DSTransaction();
  common.queryresults();
  var trnResult = common.getQueryResults();
  common.writeTestResultsToFile(specificTestData.OutputResponseFileName, trnResult, env);
}

/*module.exports.StandardPaypalPayment = StandardPaypalPayment;
function StandardPaypalPayment(commonTestData, specificTestData, env) {
  common.setgsvariables(APVariablesListObject, apVarlist);
  common.launchApPaypalUrl(commonTestData, specificTestData);
  browser.waitForVisible(apVarlist.PaypalLogin);  
  var Paypalpassworddecrypted = CryptoJS.AES.decrypt(specificTestData.Paypalpassword, key);
  var Paypalpassword = Paypalpassworddecrypted.toString(CryptoJS.enc.Utf8);  
  common.enterPaypalDetails(specificTestData.Paypalusername, Paypalpassword);
  browser.click(apVarlist.PaypalContinue);
  browser.pause(1000);
  browser.click(apVarlist.PaypalPaynow);
  browser.pause(1000);
  common.queryresults();
  var trnResult = common.getQueryResults();
  common.writeTestResultsToFile(specificTestData.OutputResponseFileName, trnResult, env);
}*/