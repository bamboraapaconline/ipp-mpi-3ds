var APVariablesList = (function () {

    function APVariablesList() {
      /*For Paypal */
      let rndm = Math.floor(Math.random() * 10000000 + 999999);    
      let mycustref = `bamboratest-${rndm}`;
      /* Access Portal Wizard */
      this.AccessPortalURL = '//*[contains(@id, "AccessPortalURL")]';
      this.sessionid = '//input[@id="txtSessionID"]';
      this.sst = '//input[@id="txtSST"]';
      this.hiddensst = '//input[@name="SST"]';
      this.sessionstorederror = '//input[@name="SessionStoredError"]';
      this.UserName = '//*[contains(@id, "UserName")]';
      this.Password = '//*[contains(@id, "Password")]';
      this.dltextfield = '[class="input-group input-group-sm"]';
      this.inputnthchild3 = 'input:nth-Child(3)';
      this.DL = '//*[contains(@id, "DL")]';
      this.CustRef = '//*[contains(@id, "CustRef")]';
      this.CustNumber = '//*[contains(@id, "CustNumber")]';
      this.Amount = '//*[contains(@id, "Amount")]';
      this.displaypopup = '[class="checkbox navbar-btn"]';
      this.SendRequest = '//*[contains(@class, "btn btn-primary")]';
      this.Reference4 = '//*[contains(@id, "Reference4")]';
      this.Reference5 = '//*[contains(@id, "Reference5")]';
      this.PaypalReference1 = '//*[contains(@id, "PP_Reference1")]';
      this.PaypalReference2 = '//*[contains(@id, "PP_Reference2")]';
      this.PaypalReference3 = '//*[contains(@id, "PP_Reference3")]';
      this.PaypalReference4 = '//*[contains(@id, "PP_Reference4")]'; 
      this.PaypalPage = '//*[contains(@class, "desktop")]';
      this.PaypalUsername = '//*[contains(@id, "login_emaildiv")]/div/input';
      this.PaypalPassword = '//*[contains(@id, "login_passworddiv")]/div/input'; 
      this.PaypalLogin = '//*[contains(@id, "btnLogin")]'; 
      this.PaypalContinue = '//*[contains(@class, "buttons reviewButton")]/button';
      this.PaypalPaynow = '//*[contains(@class, "buttons reviewButton")]/input';
      this.AddFields = '//*[contains(@id, "additionalFields")]/div/div[2]/div/button'; 
      this.AddFieldsModal = '//*[contains(@class, "modal-dialog")]';
      this.TextaddField = '//*[contains(@id, "txtFieldName")]'; 
      this.ButtonAddFields = '//*[contains(@class, "btn btn-primary btn-sm btn-add-field")]';
      this.Buttonclose = '//*[contains(@class, "modal-dialog")]/div/div[3]/button';
      this.PaypalCustref = mycustref;
      this.threeDSPayNow = '//input[@type="submit"]';
      /* Hpp Page */
      this.cardholdername = '//*[contains(@class, "form-control cardholdername")]';
      this.cardnumber = '//*[contains(@class, "form-control ccnumber")]';
      this.expm = '//*[contains(@class, "form-control expm")]';
      this.expy = '//*[contains(@class, "form-control expy")]';
      this.cvv = '//*[contains(@class, "form-control ccv small-input")]';
      this.paynow = '//*[contains(@class, "btn btn-primary submitbtn")]';
      this.sessionid = '//input[@id="txtSessionID"]';
      this.sst = '//input[@id="txtSST"]';
      this.btnquery = '//*[contains(@class, "btn btn-primary btn-sm")]';
      //this.queryresultbox = 'pre';
      this.queryresultbox = 'form';
      this.sessionstorederror = '//input[@name="SessionStoredError"]';
      this.trnqueryresults = '.DeclinedCode';
      this.demoEnv = '//*[@id="navbar"]';
      /* Direct Debit page */
      this.bankAccountHolderName = '//*[contains(@class, "form-control acctitle")]';
      this.bankAccountBSB = '//*[contains(@class, "form-control accrouting")]';
      this.bankAccountNumber = '//*[contains(@class, "form-control accno")]';
      /* Crm logon page */
      this.crmUserName = '//*[contains(@id, "txtUserName")]';
      this.crmPassword = '//*[contains(@id, "txtPassword")]';
      this.crmLogonButton = '//*[contains(@id, "btnLogOn")]';
      this.crmframecontents = '[name="fraContents"]';
      this.crmframetoolbar = '[name="fraToolbar"]';
      this.selectcompanytag = '#ddlSelectCompanyID';
      this.crmframecontentbottom = '[name="fraContentBottom"]';
      this.crmframecontenttop = '[name="fraContentTop"]';
      this.crmframemainmenu = '[name="fraMainMenu"]';
      this.crmIndexUrl = '/index.aspx';
      this.crmCardFeatures_ConfigUrl = '/ccfeatures_config.aspx';
      this.crmDifferentialSurchargeCheckBoxId = '#checkDifferentialSurcharging';
      this.crmBinLookupCheckBoxId = '#checkBinLookupService';
      this.crmCardFeaturesConfigSaveBtn = '#btnSave';
      /* View Payment */ 
      this.crmIndexUrl = '/index.aspx';
      this.crmpaymenthistory = '/account_payment_history.aspx';
      this.crmpayadditional = '//*[contains(@id, "btnAdditionalCriteria")]';
      this.crmpayccfirst6 = '//*[contains(@id, "txtCCNumberFirstDigits")]';
      this.crmpaycclast4 = '//*[contains(@id, "txtCCNumberLastDigits")]';
      //this.crmpaysearchchk = '//*[contains(@id, "chkSearchAllAccounts")]';
      this.crmpaysearchchk = '//*[contains(@class, "classCheckbox")]/input'; 
      this.crmpayccsearch = '//*[contains(@id, "btnSearch")]';//'[name="btnSearch"]';
      this.selectpaymenttype = '#ddlTrnTypeID';
      this.selectpaymentstatus = '#ddlTrnStatusID';
      this.selectpaymentactions = '#dgr__ctl3_ddlActions';   
      this.selectpayerrorcode = '//*[contains(@id, "labErrorCodeIDData")]';
      this.prmreceipt = '#txtReceipt';
      this.prmreceiptdetails = '#labReceiptData'; 
      this.submitRefundbtn = '//*[contains(@value, "Submit Refund")]'; 
      this.submitvoidbtn = '//*[contains(@value, "Submit Void")]';
      this.partialrefundamt = '#txtRefundAmount';
    }
    return APVariablesList;
  })();
  
  module.exports = APVariablesList;
  